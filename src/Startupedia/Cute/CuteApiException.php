<?php


namespace Startupedia\Cute;

/**
 * @package   Startupedia\Cute
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class CuteApiException extends \Exception
{

}