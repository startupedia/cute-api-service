<?php


namespace Startupedia\Cute\DI;

use Nette;
use Nette\DI\CompilerExtension;

/**
 * @package   Startupedia\Cute\DI
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class CuteApiExtension extends CompilerExtension
{

	protected $config = [
		'administrationUrl' => '',
		'maintenanceUrl'    => '',
		'debugger'          => '%debugMode%',
	];

	public function loadConfiguration()
	{
		$config = $this->getConfig($this->config);
		$builder = $this->getContainerBuilder();

		if (strlen($config['administrationUrl']) < 1 || strlen($config['maintenanceUrl']) < 1) {
			throw new \UnexpectedValueException("Please configure the Cute API service extension using the section '{$this->name}:' in your config file.");
		}

		$cuteApiService = $builder->addDefinition($this->prefix('service'))
			->setClass('Startupedia\Cute\CuteApiService', [
				$config['administrationUrl'],
				$config['maintenanceUrl'],
			]);

		if ($config['debugger']) {
			$cuteApiService->addSetup('Startupedia\Cute\Diagnostics\Panel::register(?, ?)',
				array('@self', '@container'));
		}
	}

}