<?php


namespace Startupedia\Cute;

use Startupedia\Cute\Soap\Request;

/**
 * @package   Startupedia\Cute
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class CuteApiService
{

	/**
	 * @var Soap\Handler
	 */
	protected $administrationApi;

	/**
	 * @var Soap\Handler
	 */
	protected $maintenanceApi;

	/**
	 * @var string
	 */
	private $url;

	/**
	 * @var string
	 */
	private $maintenanceUrl;

	/**
	 * @param string $url
	 * @param string $maintenanceUrl
	 */
	public function __construct($url, $maintenanceUrl)
	{
		$this->url = $url;
		$this->maintenanceUrl = $maintenanceUrl;
	}

	/**
	 * @param Request $request
	 * @return mixed
	 * @throws CuteApiException
	 */
	public function run(Request $request)
	{
		return $request->run($this->getAdministrationApi(), $this->getMaintenanceApi());
	}

	/**
	 * @return Soap\Handler
	 * @internal
	 */
	public function getAdministrationApi()
	{
		if (!$this->administrationApi) {
			$this->administrationApi = new Soap\Handler($this->url);
		}
		return $this->administrationApi;
	}

	/**
	 * @return Soap\Handler
	 * @internal
	 */
	public function getMaintenanceApi()
	{
		if (!$this->maintenanceApi) {
			$this->maintenanceApi = new Soap\Handler($this->maintenanceUrl);
		}
		return $this->maintenanceApi;
	}

}


