<?php


namespace Startupedia\Cute\Diagnostics;

use Nette\DI\Container;
use Nette\Object;
use Startupedia\Cute\CuteApiService;
use Tracy\Debugger;
use Tracy\Dumper;
use Tracy\IBarPanel;
use Latte;

/**
 * @package   Startupedia\Cute\Diagnostics
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class Panel extends Object implements IBarPanel
{

	/** @var CuteApiService */
	private $cuteApiService;

	/**
	 * Renders HTML code for custom tab.
	 * @return string
	 */
	public function getTab()
	{
		$latte = new Latte\Engine();
		return $latte->renderToString(__DIR__ . '/tab.latte', [
			'count' => count($this->cuteApiService->getAdministrationApi()->getCalls())
				+ count($this->cuteApiService->getMaintenanceApi()->getCalls())
		]);
	}

	/**
	 * Renders HTML code for custom panel.
	 * @return string
	 */
	public function getPanel()
	{
		$latte = new Latte\Engine();

		$latte->addFilter('formatXml', function($xml) {
			$object = @simplexml_load_string($xml);
			return @Dumper::toHtml($object, array(Dumper::COLLAPSE => TRUE, Dumper::DEPTH => 2));
		});

		$latte->addFilter('formatObject', function($object) {
			return Dumper::toHtml($object, array(Dumper::COLLAPSE => TRUE, Dumper::DEPTH => 2));
		});

		return $latte->renderToString(__DIR__ . '/panel.latte', [
			'adminCalls'       => $this->cuteApiService->getAdministrationApi()->getCalls(),
			'maintenanceCalls' => $this->cuteApiService->getMaintenanceApi()->getCalls()
		]);
	}

	/**
	 * @param CuteApiService $cuteApiService
	 * @param Container      $container
	 * @return Panel
	 */
	public static function register(CuteApiService $cuteApiService, Container $container)
	{
		$panel = new static($container);
		$panel->setCuteApiService($cuteApiService);
		Debugger::getBar()->addPanel($panel);
		return $panel;
	}

	/**
	 * @param $cuteApiService
	 */
	private function setCuteApiService(CuteApiService $cuteApiService)
	{
		$this->cuteApiService = $cuteApiService;
	}
}