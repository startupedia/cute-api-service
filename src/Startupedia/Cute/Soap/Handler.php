<?php


namespace Startupedia\Cute\Soap;

use SoapClient;
use SoapFault;
use SoapVar;
use Startupedia\Cute\CuteApiException;

/**
 * @package   Startupedia\Cute\Soap
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class Handler
{

	protected $defaultOptions = [
		'trace'      => 1,
		'exceptions' => 1,
	];

	/**
	 * @var SoapClient
	 */
	private $client;

	/**
	 * @var array
	 */
	private $calls = [];

	/**
	 * @param string $url     URL address of the API to be handled.
	 * @param array  $options Additional options.
	 */
	public function __construct($url, array $options = [])
	{
		$this->client = new SoapClient($url, array_merge($options, $this->defaultOptions));
	}

	/**
	 * @return array
	 */
	public function getCalls()
	{
		return $this->calls;
	}

	/**
	 * @param string $methodName  Name of the called method.
	 * @param string $body        XML body of the request.
	 * @param bool   $checkErrors [Optional] Should errors be checked by default?
	 *                            Default value is TRUE.
	 * @return mixed
	 * @throws CuteApiException
	 */
	public function runRequest($methodName, $body, $checkErrors = true)
	{
		try {
			$response = $this->client->$methodName(new SoapVar($body, XSD_ANYXML));

			// Logging stuff
			$this->calls[] = [
				'method'   => $methodName,
				'body'     => $body,
				'response' => $response
			];

			if ($checkErrors && $response->{$methodName . "Result"}->ErrorCode !== 0) {
				throw new CuteApiException($response->{$methodName . "Result"}->ErrorMessage);
			}
			return $response;

		} catch (SoapFault $e) {
			throw new CuteApiException(
				sprintf('An error occured while contacting cut-e API. Error: %s', $e->getMessage()),
				null,
				$e);
		}
	}
}