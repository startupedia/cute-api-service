<?php


namespace Startupedia\Cute\Soap;

use Startupedia\Cute\CuteApiException;

/**
 * @package   Startupedia\Cute\Soap
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
interface Request
{

	/**
	 * Runs the request.
	 * @param Handler $administrationHandler
	 * @param Handler $maintenanceHandler
	 * @return mixed
	 * @throws CuteApiException
	 */
	public function run(Handler $administrationHandler, Handler $maintenanceHandler);

}