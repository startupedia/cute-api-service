<?php


namespace Startupedia\Cute\Soap\Maintenance;

use Startupedia\Cute\CuteApiException;
use Startupedia\Cute\Soap\Handler;
use Startupedia\Cute\Soap\Request;

/**
 * @package   Startupedia\Cute\Soap\Maintenance
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class GetInstrumentStatus implements Request
{

	/**
	 * Marks that the instrument has not been started by the user.
	 */
	const STATUS_NOT_STARTED = 1;

	/**
	 * Marks that the instrument is being filled at the moment by the user.
	 */
	const STATUS_IN_PROGRESS = 2;

	/**
	 * Marks that the intrument is completed by the user.
	 */
	const STATUS_COMPLETE = 4;

	/**
	 * Marks that filling of the instrument has been blocked for given user.
	 */
	const STATUS_BLOCKED = 5;

	/**
	 * @var string
	 */
	private $clientId;

	/**
	 * @var string
	 */
	private $projectId;

	/**
	 * @var string
	 */
	private $candidateId;

	/**
	 * @var string
	 */
	private $instrumentId;

	/**
	 * @var string
	 */
	private $secureCode;

	/**
	 * @param string $clientId
	 * @param string $projectId
	 * @param string $candidateId
	 * @param string $instrumentId
	 * @param string $secureCode
	 */
	public function __construct($clientId, $projectId, $candidateId, $instrumentId, $secureCode)
	{
		$this->clientId = $clientId;
		$this->projectId = $projectId;
		$this->candidateId = $candidateId;
		$this->instrumentId = $instrumentId;
		$this->secureCode = $secureCode;
	}

	/**
	 * Runs the request.
	 * @param Handler $handler
	 * @param Handler $maintenanceHandler
	 * @return string
	 * @throws CuteApiException
	 */
	public function run(Handler $handler, Handler $maintenanceHandler)
	{
		$response = $maintenanceHandler->runRequest('GetInstrumentStatus',
			"<GetInstrumentStatus xmlns=\"http://tempuri.org//wsmaintenance\">
				<reqobj>
					<ClientId>$this->clientId</ClientId>
					<ProjectId>$this->projectId</ProjectId>
					<CandidateId>$this->candidateId</CandidateId>
					<InstrumentId>$this->instrumentId</InstrumentId>
					<SecureCode>$this->secureCode</SecureCode>
				</reqobj>
			</GetInstrumentStatus>");

		if ($response->GetInstrumentStatusResult && $response->GetInstrumentStatusResult->InstrumentStatus) {
			return $response->GetInstrumentStatusResult->InstrumentStatus;
		} else {
			throw new CuteApiException("Could not retrieve instrument status.");
		}
	}

}