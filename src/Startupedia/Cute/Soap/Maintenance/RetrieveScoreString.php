<?php


namespace Startupedia\Cute\Soap\Maintenance;

use Startupedia\Cute\CuteApiException;
use Startupedia\Cute\Soap\Handler;
use Startupedia\Cute\Soap\Request;

/**
 * @package   Startupedia\Cute\Soap\Maintenance
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class RetrieveScoreString implements Request
{

	/**
	 * @var string
	 */
	private $clientId;

	/**
	 * @var string
	 */
	private $projectId;

	/**
	 * @var string
	 */
	private $candidateId;

	/**
	 * @var string
	 */
	private $secureCode;

	/**
	 * @var string
	 */
	private $instrumentId;

	/**
	 * @param string $clientId
	 * @param string $projectId
	 * @param string $candidateId
	 * @param string $secureCode
	 * @param string $instrumentId
	 */
	public function __construct($clientId, $projectId, $candidateId, $secureCode, $instrumentId)
	{
		$this->clientId = $clientId;
		$this->projectId = $projectId;
		$this->candidateId = $candidateId;
		$this->secureCode = $secureCode;
		$this->instrumentId = $instrumentId;
	}

	/**
	 * Runs the request.
	 * @param Handler $handler
	 * @param Handler $maintenanceHandler
	 * @return string
	 * @throws CuteApiException
	 */
	public function run(Handler $handler, Handler $maintenanceHandler)
	{
		$response = $maintenanceHandler->runRequest('RetrieveEncScoreString',
			"<RetrieveEncScoreString xmlns=\"http://tempuri.org//wsmaintenance\">
				<reqobj>
					<ClientId>$this->clientId</ClientId>
					<ProjectId>$this->projectId</ProjectId>
					<CandidateId>$this->candidateId</CandidateId>
					<SecureCode>$this->secureCode</SecureCode>
					<InstrumentId>$this->instrumentId</InstrumentId>
				</reqobj>
			</RetrieveEncScoreString>");


		if ($response->RetrieveEncScoreStringResult && $response->RetrieveEncScoreStringResult->EncScoreString) {
			return $response->RetrieveEncScoreStringResult->EncScoreString;
		} else {
			throw new CuteApiException("Could not retrieve score string.");
		}
	}

}