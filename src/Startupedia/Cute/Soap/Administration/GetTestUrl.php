<?php


namespace Startupedia\Cute\Soap\Administration;

use Startupedia\Cute\CuteApiException;
use Startupedia\Cute\Soap\Handler;
use Startupedia\Cute\Soap\Request;

/**
 * @package   Startupedia\Cute\Soap
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class GetTestUrl implements Request
{

	/** @var string */
	private $returnUrl;

	/** @var string */
	private $clientId;

	/** @var string */
	private $projectId;

	/** @var string */
	private $instrumentId;

	/** @var string */
	private $candidateId;

	/** @var string */
	private $secureCode;

	/** @var string */
	private $requestType;

	/** @var string */
	private $languageId;

	/**
	 * @param string $returnUrl
	 * @param string $clientId
	 * @param string $projectId
	 * @param string $instrumentId
	 * @param string $candidateId
	 * @param string $secureCode
	 * @param string $requestType
	 * @param string $languageId
	 */
	public function __construct($returnUrl, $clientId, $projectId, $instrumentId, $candidateId, $secureCode,
								$requestType, $languageId)
	{
		$this->returnUrl = $returnUrl;
		$this->clientId = $clientId;
		$this->projectId = $projectId;
		$this->instrumentId = $instrumentId;
		$this->candidateId = $candidateId;
		$this->secureCode = $secureCode;
		$this->requestType = $requestType;
		$this->languageId = $languageId;
	}


	/**
	 * Runs the request.
	 * @param Handler $handler
	 * @param Handler $maintenanceHandler
	 * @return string
	 * @throws CuteApiException
	 */
	public function run(Handler $handler, Handler $maintenanceHandler)
	{
		$response = $handler->runRequest('runWSobj',
			"<runWSobj xmlns=\"http://tempuri.org//ws\">
				<reqobj>
					<ReturnUrl>$this->returnUrl</ReturnUrl>
					<ClientId>$this->clientId</ClientId>
					<ProjectId>$this->projectId</ProjectId>
					<InstrumentId>$this->instrumentId</InstrumentId>
					<CandidateId>$this->candidateId</CandidateId>
					<SecureCode>$this->secureCode</SecureCode>
					<RequestType>$this->requestType</RequestType>
					<LanguageId>$this->languageId</LanguageId>
				</reqobj>
			</runWSobj>");

		if ($response && $response->runWSobjResult && $response->runWSobjResult->RedirectUrl) {
			return $response->runWSobjResult->RedirectUrl;
		} else {
			throw new CuteApiException("Could not retrieve test url.");
		}
	}

}