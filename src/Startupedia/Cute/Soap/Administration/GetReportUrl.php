<?php


namespace Startupedia\Cute\Soap\Administration;

use Startupedia\Cute\CuteApiException;
use Startupedia\Cute\Soap\Handler;
use Startupedia\Cute\Soap\Request;

/**
 * @package   Startupedia\Cute\Soap
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class GetReportUrl implements Request
{

	/** @var string */
	private $returnUrl;

	/** @var string */
	private $clientId;

	/** @var string */
	private $projectId;

	/** @var string */
	private $candidateId;

	/** @var string */
	private $instrumentId;

	/** @var string */
	private $secureCode;

	/** @var string */
	private $requestType;

	/** @var string */
	private $languageId;

	/** @var string */
	private $reportId;

	/** @var string */
	private $normsetId;

	/** @var string */
	private $firstName;

	/** @var string */
	private $lastName;

	/**
	 * @param string $returnUrl
	 * @param string $clientId
	 * @param string $projectId
	 * @param string $candidateId
	 * @param string $instrumentId
	 * @param string $secureCode
	 * @param string $requestType
	 * @param string $languageId
	 * @param string $reportId
	 * @param string $normsetId
	 * @param string $firstName
	 * @param string $lastName
	 */
	function __construct($returnUrl, $clientId, $projectId, $candidateId, $instrumentId, $secureCode, $requestType,
						 $languageId, $reportId, $normsetId, $firstName, $lastName)
	{
		$this->returnUrl = $returnUrl;
		$this->clientId = $clientId;
		$this->projectId = $projectId;
		$this->candidateId = $candidateId;
		$this->instrumentId = $instrumentId;
		$this->secureCode = $secureCode;
		$this->requestType = $requestType;
		$this->languageId = $languageId;
		$this->reportId = $reportId;
		$this->normsetId = $normsetId;
		$this->firstName = $firstName;
		$this->lastName = $lastName;
	}


	/**
	 * Runs the request.
	 * @param Handler $handler
	 * @param Handler $maintenanceHandler
	 * @return string
	 * @throws CuteApiException
	 */
	public function run(Handler $handler, Handler $maintenanceHandler)
	{
		$response = $handler->runRequest('runWSobj',
			"<runWSobj xmlns=\"http://tempuri.org//ws\">
				<reqobj>
					<ReturnUrl>$this->returnUrl</ReturnUrl>
					<ClientId>$this->clientId</ClientId>
					<ProjectId>$this->projectId</ProjectId>
					<CandidateId>$this->candidateId</CandidateId>
					<InstrumentId>$this->instrumentId</InstrumentId>
					<SecureCode>$this->secureCode</SecureCode>
					<RequestType>$this->requestType</RequestType>
					<LanguageId>$this->languageId</LanguageId>
					<ReportId>$this->reportId</ReportId>
					<NormsetId>$this->normsetId</NormsetId>
					<FirstName>$this->firstName</FirstName>
					<LastName>$this->lastName</LastName>
					<GenderId>1</GenderId>
				</reqobj>
			</runWSobj>");

		if ($response && $response->runWSobjResult && $response->runWSobjResult->RedirectUrl) {
			return $response->runWSobjResult->RedirectUrl;
		} else {
			throw new CuteApiException('Could not retrieve redirect url.');
		}
	}
}