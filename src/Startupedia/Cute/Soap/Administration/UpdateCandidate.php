<?php


namespace Startupedia\Cute\Soap\Administration;

use Startupedia\Cute\CuteApiException;
use Startupedia\Cute\Soap\Handler;
use Startupedia\Cute\Soap\Request;

/**
 * @package   Startupedia\Cute\Soap
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class UpdateCandidate implements Request
{

	/** @var string */
	private $lastName;

	/** @var string */
	private $firstName;

	/** @var string */
	private $clientId;

	/** @var string */
	private $projectId;

	/** @var string */
	private $candidateId;

	/** @var string */
	private $secureCode;

	/**
	 * @param string $lastName
	 * @param string $firstName
	 * @param string $clientId
	 * @param string $projectId
	 * @param string $candidateId
	 * @param string $secureCode
	 */
	public function __construct($lastName, $firstName, $clientId, $projectId, $candidateId, $secureCode)
	{
		$this->lastName = $lastName;
		$this->firstName = $firstName;
		$this->clientId = $clientId;
		$this->projectId = $projectId;
		$this->candidateId = $candidateId;
		$this->secureCode = $secureCode;
	}

	/**
	 * Runs the request.
	 * @param Handler $handler
	 * @param Handler $maintenanceHandler
	 * @return boolean
	 * @throws CuteApiException
	 */
	public function run(Handler $handler, Handler $maintenanceHandler)
	{
		$response = $handler->runRequest('runWSobj',
			"<runWSobj xmlns=\"http://tempuri.org//ws\">
				<reqobj>
					<LastName>$this->lastName</LastName>
					<FirstName>$this->firstName</FirstName>
					<ClientId>$this->clientId</ClientId>
					<ProjectId>$this->projectId</ProjectId>
					<CandidateId>$this->candidateId</CandidateId>
					<SecureCode>$this->secureCode</SecureCode>
					<RequestType>cand_update</RequestType>
					<GenderId>1</GenderId>
				</reqobj>
			</runWSobj>");

		if ($response) {
			return true;
		} else {
			throw new CuteApiException("Could not update candidate.");
		}
	}
}