<?php


namespace Startupedia\Cute\Soap\Administration;

use Startupedia\Cute\CuteApiException;
use Startupedia\Cute\Soap\Handler;
use Startupedia\Cute\Soap\Request;

/**
 * @package   Startupedia\Cute\Soap
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class GetScore implements Request
{

	/** @var string */
	private $clientId;

	/** @var string */
	private $projectId;

	/** @var string */
	private $candidateId;

	/** @var string */
	private $secureCode;

	/** @var string */
	private $instrumentId;

	/** @var string */
	private $normsetId;

	/** @var string */
	private $scoreString;

	/**
	 * @param string $clientId
	 * @param string $projectId
	 * @param string $candidateId
	 * @param string $secureCode
	 * @param string $instrumentId
	 * @param string $normsetId
	 * @param string $scoreString
	 */
	public function __construct($clientId, $projectId, $candidateId, $secureCode, $instrumentId, $normsetId, $scoreString)
	{
		$this->clientId = $clientId;
		$this->projectId = $projectId;
		$this->candidateId = $candidateId;
		$this->secureCode = $secureCode;
		$this->instrumentId = $instrumentId;
		$this->normsetId = $normsetId;
		$this->scoreString = $scoreString;
	}

	/**
	 * Runs the request.
	 * @param Handler $handler
	 * @param Handler $maintenanceHandler
	 * @return string
	 * @throws CuteApiException
	 */
	public function run(Handler $handler, Handler $maintenanceHandler)
	{
		$response = $handler->runRequest('getScoresXml',
			"<getScoresXml xmlns=\"http://tempuri.org//ws\">
				<clientid>$this->clientId</clientid>
				<projectid>$this->projectId</projectid>
				<candidateid>$this->candidateId</candidateid>
				<securecode>$this->secureCode</securecode>
				<instrumentid>$this->instrumentId</instrumentid>
				<normsetid>$this->normsetId</normsetid>
				<encodedscore>$this->scoreString</encodedscore>
			</getScoresXml>", false); // getScoresXml doesn't return error code

		if ($response->getScoresXmlResult && $response->getScoresXmlResult->any) {
			return $response->getScoresXmlResult->any;
		} else {
			throw new CuteApiException("Could not retrieve score.");
		}
	}
}