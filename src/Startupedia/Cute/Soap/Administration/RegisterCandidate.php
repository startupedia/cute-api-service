<?php


namespace Startupedia\Cute\Soap\Administration;

use Startupedia\Cute\CuteApiException;
use Startupedia\Cute\Soap\Handler;
use Startupedia\Cute\Soap\Request;

/**
 * @package   Startupedia\Cute\Soap
 * @author    Filip Klimes <filipklimes@startupjobs.cz>
 * @copyright 2015, Startupedia s.r.o.
 */
class RegisterCandidate implements Request
{

	/** @var string */
	private $clientId;

	/** @var string */
	private $projectId;

	/** @var string */
	private $candidateId;

	/** @var string */
	private $secureCode;

	/**
	 * @param string $clientId
	 * @param string $projectId
	 * @param string $candidateId
	 * @param string $secureCode
	 */
	public function __construct($clientId, $projectId, $candidateId, $secureCode)
	{
		$this->clientId = $clientId;
		$this->projectId = $projectId;
		$this->candidateId = $candidateId;
		$this->secureCode = $secureCode;
	}


	/**
	 * Runs the request.
	 * @param Handler $handler
	 * @param Handler $maintenanceHandler
	 * @return boolean
	 * @throws CuteApiException
	 */
	public function run(Handler $handler, Handler $maintenanceHandler)
	{
		$response = $handler->runRequest('runWSobj',
			"<runWSobj xmlns=\"http://tempuri.org//ws\">
				<reqobj>
					<ClientId>$this->clientId</ClientId>
					<ProjectId>$this->projectId</ProjectId>
					<CandidateId>$this->candidateId</CandidateId>
					<SecureCode>$this->secureCode</SecureCode>
					<RequestType>cand_register</RequestType>
				</reqobj>
			</runWSobj>");

		if ($response) {
			return true;
		} else {
			throw new CuteApiException("Could not register candidate.");
		}
	}
}